export const SETUP_ALREADY_COMPLETE = 'Setup already complete';
export const PLEASE_RUN_SETUP = 'Please run setup';
export const SOMETHING_WENT_WRONG = 'Something went wrong';
export const NOT_CONNECTED = 'not connected';
export const SERVICE_ALREADY_REGISTERED = 'Service already registered';
export const INVALID_CODE = 'Invalid Code';
export const INVALID_STATE = 'Invalid State';
export const INVALID_FRAPPE_TOKEN = 'Invalid Frappe Token';
export const SUBMITTED_SALES_INVOICE_CANNOT_BE_UPDATED =
  'submitted Sales Invoice cannot be updated';
export const DELIVERY_NOTE_ALREADY_SUBMITTED =
  'Provided delivery note is already submitted';
export const DELIVERY_NOTE_IN_QUEUE = 'Provided delivery note is in queue';
export const SERIAL_NO_ALREADY_EXIST = 'Serial Number already exist';
export const ITEM_NOT_FOUND = 'Item not found';
export const CUSTOMER_AND_CONTACT_INVALID =
  'Provided Customer or contact email invalid';
export const SALES_INVOICE_NOT_FOUND = 'Sales Invoice not found';
export const SUPPLIER_NOT_FOUND = 'Supplier not found';
export const INVALID_WARRANTY_CLAIM_AT_POSITION =
  'Invalid Warranty Claim at position ';
export const COMPANY_NOT_FOUND_ON_FRAPPE =
  'Provided company not found, please make sure that the provided company exist on frappe';
export const DEFAULT_COMPANY_ALREADY_EXISTS = 'Default company already exists';
export const PLEASE_SETUP_DEFAULT_COMPANY = 'Please set default company';
export const INVALID_COMPANY = 'Provided Company is invalid';
export const THERE_SHOULD_BE_ONLY_ONE_DEFAULT_COMPANY =
  'There should be just 1 default company';
export const SERIAL_SHOULD_BE_EQUAL_TO_QUANTITY =
  'Quantity and provided serial_no should be equal';
export const SERIAL_NO_NOT_FOUND = 'Serial no not found';
export const INVALID_ITEM = 'Provided data has invalid item';
export const INVALID_ITEM_CODE_OR_SUPPLIER =
  'item_code and supplier not found.';
export const DELIVERY_NOTE_ALREADY_EXISTS = 'Delivery note already exist';
export const INVALID_HTTP_METHOD = 'Invalid HTTP Request Method';
export const INVALID_REQUEST = 'Invalid Request';
export const SALES_INVOICE_MANDATORY = 'Sales invoice url query is mandatory.';
